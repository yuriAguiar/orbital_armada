**Description**

Orbital Armada is a prototype Star Fox like on rails shooter game made using the core features of the Unity Engine and C#. Please note that the game is still in early active development  and only access to the project repo is possible at the moment. The player spaceship path was created using the Unity Timeline tool.
In the game you need to defend the planet from an invasion and get the highest score that you can while flying through the planet's surface. Future features include more and longer levels, moving enemies and bosses.

**Installation**

At the present moment you can only play or test the game through the Unity Engine. To do that simply clone the project to your PC and open it in Unity to play or mess around as you desire.